* 21-11-2019: Found LED in new products section. Started project, did research on UVC and DNA. Pretty cool stuff.
* 22-11-2019: Sent mail via tutanota to Inolux but no response.
* 23-11-2019: Needed application circuit for the UVC LED, sent using cock.li
* 24-11-2019: Received initial mail from Inolux Corporation on cock.li, sent another mail with the exact details.
* 27-11-2019: Got the mail from Inolux engineer.
* 28-11-2019: Saw new Vishay UVC LED Ditched Inolux for Vishay.
* 10-12-2019: Designed first LED Driver PCB.
* 12-12-2019: Started battery charging circuit design.
* 24-12-2019: First PCB design uploaded to JLCPCB.
* 30-12-2019: Everything almost done. Final touches.
* 31-12-2019: Made a lot of modifications.
* 01-01-2020: Found the logo for Lily and UV Warning.
* 04-01-2020: Final PCB done.
* 08-01-2020: Waited for the watch to be done but plan change so uploaded.