## Design Notes ##
[Reference layout](https://www.st.com/en/applications/wearable/smart-watches.html)

## Microcontroller Platforms ##
Microcontroller platforms considered are MSP430, STM32L, and STM8L.

### Requirements ###
* Low operating current
* Hot swapping
* ADC with good resolution
* ADC with more than 5 channels
* Communication protocol with best power efficiency
* LCD interface  
* 

### Comparison ###
Some comparisions.

#### Communication protocols ####
* Power Efficiency: Has to be high, with reference to this [paper](http://cc.oulu.fi/~kmikhayl/site-assets/pdfs/2012_NTMS.pdf) it is clearly evident that **SPI wins**
* Data Transfer: 
* 
* 
