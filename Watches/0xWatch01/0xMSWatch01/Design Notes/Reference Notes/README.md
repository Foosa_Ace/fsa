### Reference Notes ###

* [Hot swapping](https://electronics.stackexchange.com/questions/291226/what-is-the-simplest-hot-swappable-bus-protocol)

#### Communication ####

* [I2C](https://www.nxp.com/docs/en/user-guide/UM10204.pdf) - Official I2C Protocol
* [I2C](https://www.bluedot.space/tutorials/how-many-devices-can-you-connect-on-i2c-bus/) - I2C Pull up resistor, useful link.
