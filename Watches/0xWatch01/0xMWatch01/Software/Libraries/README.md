# RTC_DS1307

Credit goes to Seeed Studio. 

* [GitHub](https://github.com/Seeed-Studio/RTC_DS1307)

# Servo Library for Arduino

Credit goes to Michael Margolis.

* [GitHub](https://github.com/arduino-libraries/Servo)

# Wire Library for Arduino

* [GitHub](https://github.com/arduino/ArduinoCore-avr/tree/master/libraries/Wire)