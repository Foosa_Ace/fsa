# MWatch - Motor Watch

Control large AC motors using a Servo motor instead of a relay.

## Software 

* [Arduino IDE](https://www.arduino.cc/en/Main/Software) 

### Hardware

```
1) ATmega328p
2) USBasp Programmer
3) Button
4) CP2102 USB to UART Bridge Controller
5) Perfboard 20x20 cm
```