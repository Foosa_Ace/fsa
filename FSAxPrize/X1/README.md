![FSAxPRIZE](Assets/fsaxprize.svg.png)

### Goal ###

**Optimal way to reduce vegetable waste on roadside markets**
![Market](https://tci.cornell.edu/wp-content/uploads/2019/02/IMG_3203_cropped-1024x683.jpg)

### Summary ###

In India we have a very interesting weekly vegetable market system across all the major cities. We often find uneducated vegetable vendors selling on the roadside and I'm sure we are all very well aware of the vegetable waste next day(on the roads). The goal here is the "educated" need to find an optimal solution to reduce vegetable waste on roadside markets which can assist the uneducated and improve the overall scenario. The best solution wins the prize. 

### Dates ###

* August 15 2019 - Discussion
* August 20 2019 - Review
* September 2019 - Market testing

### Prize ###
₹ 1000

### Market testing ideas ###
All suggestions welcome.

### Contact ###
foosa@tuta.io
