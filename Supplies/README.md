# Supplies #

### Transistors ###
#### BJT ####
* [BC307B](https://www.onsemi.com/pub/Collateral/BC307-D.PDF) x 1
* [BC251B](https://pdf1.alldatasheet.com/datasheet-pdf/view/86519/MOTOROLA/BC251B.html) x 1
* [BD240C](https://www.mouser.com/datasheet/2/149/BD240-196168.pdf) x 1

#### JFET ###
* [BFW10](https://pdf1.alldatasheet.com/datasheet-pdf/view/77365/MOTOROLA/BFW10.html)

### Diodes ###
#### General Purpose Diodes ####
* [1N4007](https://www.mouser.com/datasheet/2/149/1N4007-888322.pdf) x 10

#### Fast Switching Diodes ####
* [1N4148](https://www.vishay.com/docs/81857/1n4148.pdf)



### Rectifiers ###

* [D5SBA20](https://www.digchip.com/datasheets/download_datasheet.php?id=260508&part-number=D5SBA20) x 1 - General purpose rectifier

### Voltage Regulators ###
#### Linear Voltage Regulators ###

#### 5V ####
###### Positive ######
* [GL7805](https://www.datasheets360.com/part/detail/gl7805/1821955753755556131/) x 2
* [GL7905](https://www.alldatasheet.com/datasheet-pdf/pdf/128111/HYNIX/GL7905.html) x 1
* [L7805](https://www.mouser.in/datasheet/2/389/cd00000444-1795274.pdf) x 1
###### Negative ######
* [L7905CV](https://www.mouser.in/datasheet/2/389/cd00000450-1795462.pdf) x 2
* [AN7905T](https://www.alldatasheet.com/datasheet-pdf/pdf/13502/PANASONIC/AN7905T.html) x 1
* [UA7905](https://www.digchip.com/datasheets/parts/datasheet/477/UA7905.php) x 1

#### 9V ####
* [L7809CV](https://www.mouser.in/datasheet/2/389/cd00000444-1795274.pdf) x 1

#### 12V ####
###### Positive ######
* [AN7812](https://industrial.panasonic.com/content/data/SC/ds/ds4/AN7800_E_discon.pdf) x 1 
* [L7812CV](https://www.mouser.in/datasheet/2/389/cd00000444-1795274.pdf) x 1 
###### Negative ######
* [KA7912](https://www.mouser.in/datasheet/2/308/fairchild%20semiconductor_ka7905-1191588.pdf) x 1
* [L7912CV](https://www.mouser.in/datasheet/2/389/cd00000450-1795462.pdf) x 1
* [MC7912CTT](https://www.mouser.in/datasheet/2/308/MC7900-D-1773699.pdf) x 1

#### 15V ####
###### Positive ######
* [AN7815](https://industrial.panasonic.com/content/data/SC/ds/ds4/AN7800_E_discon.pdf) x 1 
* [KIA7815P](https://www.kec.co.kr/data/databook/pdf/KIA/Eng/KIA7805AP~KIA7824AP.pdf) x 2
* [L7815CV](https://www.mouser.in/datasheet/2/389/cd00000444-1795274.pdf) x 1
###### Negative ######
* [MC7915CT](https://www.mouser.in/datasheet/2/308/MC7900-D-1773699.pdf) x 3 
* [GL7915](https://www.digchip.com/datasheets/download_datasheet.php?id=2492686&part-number=GL7915) x 2
* [L7915CV](https://www.mouser.in/datasheet/2/389/cd00000450-1795462.pdf) x 1

#### 18V ####
* [MC7918CT](https://www.mouser.in/datasheet/2/308/mc7900-d-1193345.pdf) x 2

#### 24V ####
* [AN7824](https://www.alldatasheet.com/view.jsp?Searchword=AN7824&sField=4)


#### Adjustable ####
###### Positive ######
* [LM317](https://media.digikey.com/pdf/Data%20Sheets/Fairchild%20PDFs/KA317,LM317.pdf) x 1
###### Negative ######
* [LM337T](http://www.ti.com/lit/ds/symlink/lm337-n.pdf) x 1

























