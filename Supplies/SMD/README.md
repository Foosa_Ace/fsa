# Passives

## Inductors 
* [MLZ2012M220WT000](https://www.mouser.in/ProductDetail/TDK/MLZ2012M220WT000?qs=%2FPzWLGNeQ%252BikVJiRseHlfw%3D%3D) x2
* [SRF1260-330M](https://www.mouser.in/ProductDetail/Bourns/SRF1260-330M?qs=%2Fha2pyFaduh3UNATjz4t6%252BD9iyYhfBHZ6m12KkOtjnS7fKXnJPLAKQ%3D%3D) x2


## Capacitors
* [0805X475K6R3CT](https://www.mouser.in/ProductDetail/Walsin/0805X475K6R3CT?qs=%2Fha2pyFadujLzkVwElOeuorz9mCk20SPjivd9H2lzS54ibrwRLW%2F1A%3D%3D) x2 4.7 uF, 6.3 VDC, X5R, 
* [C2012X7R1H224K125AA](https://www.mouser.in/ProductDetail/TDK/C2012X7R1H224K125AA?qs=%2Fha2pyFaduiVC02s%252Bc%2FwRfH18yJhy%2F8YA%2FyiOZZ03LP%2FEohvYSo87%2F0qOv9ZnIGY) x4 0.22 uF, 50 VDC, X7R 
* [C1206C475K5RACTU](https://www.mouser.in/ProductDetail/KEMET/C1206C475K5RACTU?qs=eTOzTKnOMhYOc4dq0S20Rg%3D%3D) x2, 4.7uF, 50 VDC, X7R	
* [TMK212BBJ106MGHT](https://www.mouser.in/ProductDetail/Taiyo-Yuden/TMK212BBJ106MGHT?qs=%2Fha2pyFaduhw4FYMlb5JGT3rIlc6SuujIF4%252BaPlokWvT48Ct7jkXYQ%3D%3D) x10, 10uF, 25 VDC, X5R, 2012

## Resistors
* [CRGP0805F10R](https://www.mouser.in/ProductDetail/TE-Connectivity-Holsworthy/CRGP0805F10R?qs=%2Fha2pyFaduiVCY45aEAypEBEYpWTDocY88fLQ1Ntj%2FULb6qHArHzPQ%3D%3D) x3
* [ERJ-2GEJ103X](https://www.mouser.in/ProductDetail/Panasonic/ERJ-2GEJ103X?qs=%2Fha2pyFaduiAByf70Py6Tejmb1AKc5qCaFEqd9EUQ8lL7kdh0BhCYw%3D%3D) x10, 10 kOhms, 50 V, 100 mW, 1005
* [ERJ-2RKF1000X](https://www.mouser.in/ProductDetail/Panasonic/ERJ-2RKF1000X?qs=%2Fha2pyFadujciewGcSbxYDI1vAOI6jj%252BTlUt5yoPpvntvTeB78ujCw%3D%3D) x10, 100 Ohms, 50 V, 100 mW, 1005
* [RC0402JR-0750RL](https://www.mouser.in/ProductDetail/Yageo/RC0402JR-0750RL?qs=%2Fha2pyFadugNN0LzwXn2qBFJRQDD5oGHhqHMhAgJD5qwvpgjYQKANQ%3D%3D) x10, 50 Ohms, 50V, 62.5 mW, 1005	 	

## Negative Temperature Coefficient Thermistors
* [NCP15XH103F03RC](https://www.mouser.in/ProductDetail/Murata-Electronics/NCP15XH103F03RC?qs=%2Fha2pyFaduhrTQZRTIpElYBvoj%252BbwZt0iVuO4qgN4AksYNy1oFyfBA%3D%3D) x5, 3380 K, 10 kOhms, 100 mW, 1005		 

# Active

## PMIC
* [LM3481MM/NOPB](https://www.mouser.in/ProductDetail/Texas-Instruments/LM3481MM-NOPB?qs=%2Fha2pyFaduj24k4El3yITi410c9mGJX%252Bzy2qlY%252BXqlhqufey8el1bg%3D%3D) x2 

## LED Driver
* [XC9116B02AMR-G](https://www.mouser.in/ProductDetail/Torex-Semiconductor/XC9116B02AMR-G?qs=%2Fha2pyFadujuKMXYiAv2%252Bp2S%2FhlUEWfD9NuBWCxqbbv5IpZvVEY2NQ%3D%3D) x1

## MOSFET
* [DMN3009LFV-7](https://www.mouser.in/ProductDetail/Diodes-Incorporated/DMN3009LFV-7?qs=%2Fha2pyFaduhF8PHasEx1gdIS2gC5Tsim77wvKkJpOmiqC0Yiu5QhRA%3D%3D) x1 
* [CSD18543Q3AT](https://www.mouser.in/ProductDetail/Texas-Instruments/CSD18543Q3AT?qs=%2Fha2pyFadugNZtiDUDiULz36IR%252BSo8BI04ZAv%252BFPAX7i3zMeD6dtdg%3D%3D) x2

## Schottky Diodes
* [STPS3170AFN](https://www.mouser.in/ProductDetail/STMicroelectronics/STPS3170AFN?qs=Cb2nCFKsA8pM4tLC7PCfCQ%3D%3D) x2

## Microcontrollers
* [ESP8266EX](https://www.mouser.in/ProductDetail/Espressif-Systems/ESP8266EX?qs=%2Fha2pyFadujcHGkZXWDHgBDVCt8PC4Ad6HL4U7Mx202YaDwxTots9g%3D%3D) x3 