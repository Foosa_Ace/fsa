# Modules 
Contains documentations for the modules

### Modules Done 

1. Battery Management System
- LiPoC1(MCP73833)

2. LED Driver
- LEDD1a(XC9116)

3. PWM Source
- PWM1(ATtiny85)

### Modules under progress

1. Accurate Stepper Motor Driver: Improving NEMA 17 steppers
- ASMD1


2. Battery Management System: Battery charging and charge estimation in percentage.
- BMS1 


### TODO Modules

1. LED Driver
- LEDD1b(Viper12A) - Cheaper simpler PWM alternative.

2. Battery Management
- Battery charging and charge estimation in percentage.

