### Accurate Stepper Motor Driver

##### Quick links

1. [How accurate is microstepping really?](https://hackaday.com/2016/08/29/how-accurate-is-microstepping-really/)
2. [Stepper motor accuracy](https://www.applied-motion.com/news/2015/10/stepper-motor-accuracy)
3. [Microstepping myths and realities](http://www.micromo.com/microstepping-myths-and-realities)

## Things to consider while designing:

1. Microstepping reduces torque. If the microstep is large the torque won't move the mechanical load.
2. Motors have an internal mechanical load. (Holding Torque = Detent Torque + Internal Friction)
