# High Voltage Flyback Boost Converter #

## Steps ##
1) Working principle of Flyback topology
2) Flyback transformer design
3) Additional components

## Progress ##
1) Working principle of Flyback topology


## Resources ##
* [USB Plasma Ball Teardown](https://blog.adafruit.com/2016/12/01/usb-plasma-ball-teardown-with-rf-energy-demo/)